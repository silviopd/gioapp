import { Text, View, ScrollView } from "react-native"
import Constants from "expo-constants"
import CollapseItems from "./CollapseItems"

const Main = () => {
  return (
    <View
      style={{
        marginTop: Constants.statusBarHeight,
        flexGrow: 1,
        flex: 1,
        marginBottom: Constants.statusBarHeight,
      }}
    >
      <Text>React repository aplications</Text>
      <ScrollView>
        <View style={{ marginBottom: Constants.statusBarHeight }}>
          <CollapseItems />
        </View>
      </ScrollView>
      <View style={{ position: "absolute", left: 0, right: 0, bottom: 0 }}>
        <Text style={{ textAlign: "center" }}>My fixed footer</Text>
      </View>
    </View>
  )
}

export default Main
