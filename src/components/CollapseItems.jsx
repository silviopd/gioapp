import {
  Collapse,
  CollapseHeader,
  CollapseBody,
} from "accordion-collapse-react-native"
import { Text, View } from "react-native"
import AGA from "./AGA"
import Personales from "./Personales"
import FormulasPrincipales from "./FormulasPrincipales"

const CollapseItems = () => {
  const items = [
    { title: "Datos AGA", body: <AGA /> },
    { title: "Datos Personales", body: <Personales /> },
    { title: "Formulas", body: <FormulasPrincipales /> },
  ]

  const renderItem = (item, index) => (
    <Collapse key={index}>
      <CollapseHeader>
        <Text>{item.title}</Text>
      </CollapseHeader>
      <CollapseBody>
        <Text>{item.body}</Text>
      </CollapseBody>
    </Collapse>
  )

  return items.map(renderItem)
}

export default CollapseItems
