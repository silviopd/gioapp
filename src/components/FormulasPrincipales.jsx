import { useState } from "react"
import {
  SafeAreaView,
  StyleSheet,
  TextInput,
  Text,
  View,
  ScrollView,
  Platform,
} from "react-native"

const FormulasPrincipales = () => {
  const [v_edad, onChangeEDAD] = useState(null)
  const [v_peso, onChangePESO] = useState(null)
  const [v_sexo, onChangeSEXO] = useState("M")

  const variablePersonales = [
    {
      variable: v_edad,
      change: onChangeEDAD,
      placeholder: "Edad",
      type: "numeric",
      unit: "Años",
    },
    {
      variable: v_peso,
      change: onChangePESO,
      placeholder: "Peso",
      type: "numeric",
      unit: "Kg",
    },
    {
      variable: v_sexo,
      change: onChangeSEXO,
      placeholder: "Sexo",
      type: "text",
      unit: "",
    },
  ]

  const renderItem = (item, index) => (
    <View style={styles.item} key={index}>
      <TextInput
        style={styles.input}
        onChangeText={item.change}
        value={item.variable}
        placeholder={item.placeholder}
        keyboardType={item.type}
      />
      <View style={{ flexDirection: "row", alignSelf: "center" }}>
        <Text style={styles.marginR}>{item.placeholder}</Text>
        <Text>{item.unit}</Text>
      </View>
    </View>
  )

  return (
    <ScrollView>
      <SafeAreaView style={styles.content}>
        {variablePersonales.map(renderItem)}
      </SafeAreaView>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  marginR: {
    marginRight: 5,
  },
  marginT: {
    marginTop: 10,
  },
  content: {
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
    justifyContent: "center",
    border: "1px solid black",
  },
  item: {
    width: Platform.OS === "web" ? 200 : "33%",
  },
})

export default FormulasPrincipales
