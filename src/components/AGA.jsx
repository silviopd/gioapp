import { useState } from "react"
import {
  SafeAreaView,
  StyleSheet,
  TextInput,
  Text,
  View,
  ScrollView,
  Platform,
} from "react-native"

const AGA = () => {
  const [v_pH, onChangePH] = useState(null)
  const [v_pCO2, onChangePCO2] = useState(null)
  const [v_pO2, onChangePO2] = useState(null)
  const [v_Hct, onChangeHCT] = useState(null)
  const [v_NA, onChangeNA] = useState(null)
  const [v_K, onChangeK] = useState(null)
  const [v_Cl, onChangeCL] = useState(null)
  const [v_CA, onChangeCA] = useState(null)
  const [v_Mg, onChangeMG] = useState(null)
  const [v_Glu, onChangeGLU] = useState(null)
  const [v_Lac, onChangeLAC] = useState(null)
  const [v_TCO2, onChangeTO2] = useState(null)
  const [v_tHb, onChangeTHB] = useState(null)
  const [v_O2Hb, onChangeO2HB] = useState(null)
  const [v_COHb, onChangeCOHB] = useState(null)
  const [v_MetHb, onChangeMETHB] = useState(null)
  const [v_HHb, onChangeHHB] = useState(null)
  const [v_tBil, onChangeTBIL] = useState(null)
  const [v_nCa, onChangeNCA] = useState(null)
  const [v_nMg, onChangeNMG] = useState(null)
  const [v_Gap, onChangeGAP] = useState(null)
  const [v_CaMg, onChangeCAMG] = useState(null)
  const [v_pHTC, onChangePHTC] = useState(null)
  const [v_pCO2TC, onChangePCO2TC] = useState(null)
  const [v_pO2TC, onChangePO2TC] = useState(null)
  const [v_SO2, onChangeSO2] = useState(null)
  const [v_BEecf, onChangeBEECF] = useState(null)
  const [v_Beb, onChangeBEB] = useState(null)
  const [v_SBC, onChangeSBC] = useState(null)
  const [v_HCO3, onChangeHCO3] = useState(null)
  const [v_RI, onChangeRI] = useState(null)
  const [v_pO2FIO2, onChangePO2FIO2] = useState(null)
  const [v_O2Cap, onChangeO2CAP] = useState(null)
  const [v_O2Ct, onChangeO2CT] = useState(null)
  const [v_A, onChangeA] = useState(null)
  const [v_AaDO2, onChangeAADO2] = useState(null)
  const [v_aA, onChangeAA] = useState(null)
  const [v_CcO2, onChangeCCO2] = useState(null)
  const [v_CaO2, onChangeCAO2] = useState(null)

  const variableAGA = [
    {
      variable: v_pH,
      change: onChangePH,
      placeholder: "pH",
      type: "numeric",
      unit: "",
    },
    {
      variable: v_pCO2,
      change: onChangePCO2,
      placeholder: "pCO2",
      type: "numeric",
      unit: "mmHg",
    },
    {
      variable: v_pO2,
      change: onChangePO2,
      placeholder: "pO2",
      type: "numeric",
      unit: "mmHg",
    },
    {
      variable: v_Hct,
      change: onChangeHCT,
      placeholder: "Hct",
      type: "numeric",
      unit: "%",
    },
    {
      variable: v_NA,
      change: onChangeNA,
      placeholder: "NA++",
      type: "numeric",
      unit: "mmol/L",
    },
    {
      variable: v_K,
      change: onChangeK,
      placeholder: "K+",
      type: "numeric",
      unit: "mmol/L",
    },
    {
      variable: v_Cl,
      change: onChangeCL,
      placeholder: "Cl-",
      type: "numeric",
      unit: "mmol/L",
    },
    {
      variable: v_CA,
      change: onChangeCA,
      placeholder: "CA++",
      type: "numeric",
      unit: "mmol/L",
    },
    {
      variable: v_Mg,
      change: onChangeMG,
      placeholder: "Mg++",
      type: "numeric",
      unit: "mmol/L",
    },
    {
      variable: v_Glu,
      change: onChangeGLU,
      placeholder: "Glu",
      type: "numeric",
      unit: "mg/dL",
    },
    {
      variable: v_Lac,
      change: onChangeLAC,
      placeholder: "Lac",
      type: "numeric",
      unit: "mmol/L",
    },
    {
      variable: v_TCO2,
      change: onChangeTO2,
      placeholder: "TCO2",
      type: "numeric",
      unit: "mmol/L",
    },
    {
      variable: v_tHb,
      change: onChangeTHB,
      placeholder: "tHb",
      type: "numeric",
      unit: "g/dL",
    },
    {
      variable: v_O2Hb,
      change: onChangeO2HB,
      placeholder: "O2Hb",
      type: "numeric",
      unit: "%",
    },
    {
      variable: v_COHb,
      change: onChangeCOHB,
      placeholder: "COHb",
      type: "numeric",
      unit: "%",
    },
    {
      variable: v_MetHb,
      change: onChangeMETHB,
      placeholder: "MetHb",
      type: "numeric",
      unit: "%",
    },
    {
      variable: v_HHb,
      change: onChangeHHB,
      placeholder: "HHb",
      type: "numeric",
      unit: "%",
    },
    {
      variable: v_tBil,
      change: onChangeTBIL,
      placeholder: "tBil",
      type: "numeric",
      unit: "mg/dL",
    },
    {
      variable: v_nCa,
      change: onChangeNCA,
      placeholder: "nCa",
      type: "numeric",
      unit: "mmol/L",
    },
    {
      variable: v_nMg,
      change: onChangeNMG,
      placeholder: "nMg",
      type: "numeric",
      unit: "mmol/L",
    },
    {
      variable: v_Gap,
      change: onChangeGAP,
      placeholder: "Gap",
      type: "numeric",
      unit: "mmol/L",
    },
    {
      variable: v_CaMg,
      change: onChangeCAMG,
      placeholder: "Ca++/Mg++",
      type: "numeric",
      unit: "mol/mol",
    },
    {
      variable: v_pHTC,
      change: onChangePHTC,
      placeholder: "pH(TC)",
      type: "numeric",
      unit: "",
    },
    {
      variable: v_pCO2TC,
      change: onChangePCO2TC,
      placeholder: "pCO2(TC)",
      type: "numeric",
      unit: "mmHg",
    },
    {
      variable: v_pO2TC,
      change: onChangePO2TC,
      placeholder: "pO2(TC)",
      type: "numeric",
      unit: "mmHg",
    },
    {
      variable: v_SO2,
      change: onChangeSO2,
      placeholder: "SO2%",
      type: "numeric",
      unit: "",
    },
    {
      variable: v_BEecf,
      change: onChangeBEECF,
      placeholder: "BE-ecf",
      type: "numeric",
      unit: "mmol/L",
    },
    {
      variable: v_Beb,
      change: onChangeBEB,
      placeholder: "Be-b",
      type: "numeric",
      unit: "mmol/L",
    },
    {
      variable: v_SBC,
      change: onChangeSBC,
      placeholder: "SBC",
      type: "numeric",
      unit: "mmol/L",
    },
    {
      variable: v_HCO3,
      change: onChangeHCO3,
      placeholder: "HCO3-",
      type: "numeric",
      unit: "mmol/L",
    },
    {
      variable: v_RI,
      change: onChangeRI,
      placeholder: "RI",
      type: "numeric",
      unit: "",
    },
    {
      variable: v_pO2FIO2,
      change: onChangePO2FIO2,
      placeholder: "pO2/FIO2",
      type: "numeric",
      unit: "mmHg",
    },
    {
      variable: v_O2Cap,
      change: onChangeO2CAP,
      placeholder: "O2Cap",
      type: "numeric",
      unit: "mL/dL",
    },
    {
      variable: v_O2Ct,
      change: onChangeO2CT,
      placeholder: "O2Ct",
      type: "numeric",
      unit: "mL/dL",
    },
    {
      variable: v_A,
      change: onChangeA,
      placeholder: "A",
      type: "numeric",
      unit: "mmHg",
    },
    {
      variable: v_AaDO2,
      change: onChangeAADO2,
      placeholder: "A-aDO2",
      type: "numeric",
      unit: "mmHg",
    },
    {
      variable: v_aA,
      change: onChangeAA,
      placeholder: "a/A",
      type: "numeric",
      unit: "",
    },
    {
      variable: v_CcO2,
      change: onChangeCCO2,
      placeholder: "CcO2",
      type: "numeric",
      unit: "mL/dL",
    },
    {
      variable: v_CaO2,
      change: onChangeCAO2,
      placeholder: "CaO2",
      type: "numeric",
      unit: "mL/dL",
    },
  ]

  const renderItem = (item, index) => (
    <View style={styles.item} key={index}>
      <TextInput
        style={styles.input}
        onChangeText={item.change}
        value={item.variable}
        placeholder={item.placeholder}
        keyboardType={item.type}
      />
      <View style={{ flexDirection: "row", alignSelf: "center" }}>
        <Text style={styles.marginR}>{item.placeholder}</Text>
        <Text>{item.unit}</Text>
      </View>
    </View>
  )

  return (
    <ScrollView>
      <SafeAreaView style={styles.content}>
        {variableAGA.map(renderItem)}
      </SafeAreaView>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  marginR: {
    marginRight: 5,
  },
  marginT: {
    marginTop: 10,
  },
  content: {
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
    justifyContent: "center",
    border: "1px solid black",
  },
  item: {
    width: Platform.OS === "web" ? 200 : "33%",
  },
})

export default AGA
